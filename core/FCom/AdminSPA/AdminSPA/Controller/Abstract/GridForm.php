<?php

abstract class FCom_AdminSPA_AdminSPA_Controller_Abstract_GridForm extends FCom_AdminSPA_AdminSPA_Controller_Abstract
{
    use FCom_AdminSPA_AdminSPA_Controller_Trait_Grid;
    use FCom_AdminSPA_AdminSPA_Controller_Trait_Form;
}