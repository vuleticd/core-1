define(['text!sv-comp-form-layout-tpl'], function (layoutTpl) {
    return {
        template: layoutTpl,
        props: ['form', 'layout'],
        data: function () {
            return {
            }
        }
    }
});