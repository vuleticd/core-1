define(['text!sv-page-catalog-products-form-merchandizing-tpl'], function (tabMainTpl) {
    return {
        template: tabMainTpl,
        props: ['form'],
        data: function () {
            return {
                dict: SvAppData
            }
        }
    }
});